//derived from bitbin - http://bit.s3m.us by coda

(function(){
  window.SynthVoice = function() {
    this.reset = function() {
       this.enabled=false;
       this.phase=0;
       this.note=0;
       this.wave=0;
       this.volume=0;
       this.ramp_volume=0;
       this.vibrato=0;
       this.vibrato_phase=0;
       this.porta_dest=0;
       this.detune=0;
       this.chid=-1;
    };
    this.reset();
  }    
  window.SynthChannel = function() {
    this.reset = function() {
      this.mute=false;
      this.volume=127;
      this.exp=127;
      this.pitch=0;
      this.pitchwheelrange=2;
      this.rpn=0;
      this.program=0;
    };
    this.reset();
  }

  window.Synth = function(sample_rate,voices) {
    this.sample_rate = sample_rate;
    this.v = []
    this.c = []
    this.MAX_VOICES = voices;
    this.MAX_CHANNELS = 16;
    this.outL = 0;
    this.outR = 0;

    for (var i = 0; i < this.MAX_VOICES; i ++) {
      this.v.push(new SynthVoice());
    }
    for (var i = 0; i < this.MAX_CHANNELS; i ++) {
      this.c.push(new SynthChannel());
    }

    this.play = function() {
      var left = 0;
      for (var i=0;i<this.MAX_VOICES;i++) {
        var vce = this.get_voice(i);
        if (vce.enabled) {
          var chan = this.get_channel(vce.chid);
          var wave = waves[vce.wave];
          var wave_loop = waveLoop[vce.wave];
          var wave_trans = wavetrans[vce.wave];
          if (vce.ramp_volume < vce.volume) vce.ramp_volume+=0.125;
          if (vce.ramp_volume > vce.volume) vce.ramp_volume-=0.125;
          left += (waves[ vce.wave ][ Math.floor(vce.phase) ] * vce.ramp_volume / 127.0 / 4 * chan.volume/127.0 * chan.exp/127.0) / 8.0;
          var sr = 8372 * Math.pow(2.0, (vce.note + chan.pitch + wave_trans - 12*7 + vce.vibrato + vce.detune) / 12.0);
          vce.phase += sr / this.sample_rate;
          while (vce.phase >= wave.length) {
            vce.phase -= wave.length;
            if (!wave_loop) vce.phase = wave.length - 1;          
          }
        }
      }
      if(left > 1.0) left = 1;
      if(left < -1.0) left = -1;
      this.outL = left;
      this.outR = left;
    };

    this.reset_channel = function(cid) {
      this.get_channel(cid).reset();
    };

    this.get_voice = function(vid) {
      return this.v[vid];
    }

    this.get_channel = function(cid) {
      return this.c[cid];
    }

    this.get_free_voice = function() {
      for (var idst = 0; idst < this.MAX_VOICES; idst++) {
        var vce = this.get_voice(idst);
        if (!vce.enabled) {
          return vce;
        }
      }
      return null;
    };

    this.find_voice = function(ch,note) {
      for (var idst = 0; idst < this.MAX_VOICES; idst++) {
        var vce = this.get_voice(idst);
        if (vce.enabled && vce.chid == ch && vce.note == note) {
          return idst;
        }
      }
      return null;
    };

    this.kill_voice = function(vid) {
       this.get_voice(vid).reset();
    };

    this.kill_all_voices = function() {
      for (var i = 0; i < this.MAX_VOICES; i ++ ) {
        this.kill_voice(i);
      }
    }

    this.reset_all_channels = function() {
      for (var i = 0; i < this.MAX_CHANNELS; i++) {
        this.reset_channel(i);
      }
    }

    this.midi_event =   function ( ev ) {

      var status = ev.data[0];
      var shi = (status & 0xF0) >> 4;
      var ch = status & 0xF;
      if (shi == 0x9 && ev.data[2] > 0) {
        var notenum = ev.data[1];
        var velo = ev.data[2];
        var vce = this.get_free_voice();

        if (vce != null) {
          if (ch == 9) {
            var drm = drummap[notenum];
            if (drm) {
              vce.enabled = true;
              vce.chid = ch;
              vce.volume = velo;
              vce.note = drm[1];
              vce.wave = drm[0];              
            }
          }
          else {
            vce.enabled = true;
            vce.chid = ch;
            vce.volume = velo;
            vce.note = notenum;
            vce.wave = 1; 
            vm = voicemap[this.get_channel(ch).program];
            if (vm != null) {
              vce.wave = vm;
            }
          }
        }
      }
      else if (shi == 0x8 || (shi == 0x9 && ev.data[2] == 0)) {
        var notenum = ev.data[1];
        if (ch == 9) {
          var drm = drummap[notenum];
          if (drm) {
            notenum = drm[1];
          }
        }

        var vid = null;
        while((vid = this.find_voice(ch,notenum)) != null) {
          if (vid != null) {
            this.kill_voice(vid);
          }
        }
      }
      else if (shi == 0xB) {
        var chan = this.get_channel(ch);
        if (ev.data[1] == 0x7) {
          chan.volume = ev.data[2];
        }
        else if (ev.data[1] == 11) {
          chan.exp = ev.data[2];
        }
        else if (ev.data[1] == 100) {
          chan.rpn = (chan.rpn & 0x3F80) + ev.data[2]
        }
        else if (ev.data[1] == 101) {
          chan.rpn = (chan.rpn & 0x7F) + (ev.data[2] << 7);
        }
        else if (ev.data[1] == 6) { 
          if (chan.rpn == 0) {
            chan.pitchwheelrange = ev.data[2];
          }
        }
        else if (ev.data[1] == 123) {
          this.kill_all_voices();
        }
      }
      else if (shi == 0xE) {
        var chan = this.get_channel(ch);
        pval = ev.data[1] + (ev.data[2] << 7);
        pval = pval - 0x2000;
        pval = (pval / 0x1FFF) * chan.pitchwheelrange;
        chan.pitch = pval;
      }
      else if (shi == 0xC) {
        this.get_channel(ch).program = ev.data[1];
      }
    };

  }

})();