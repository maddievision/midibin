(function(){

  //midi processor
  window.receiveMIDI = function( ev ) {
    synth.midi_event(ev);
  }

  //initialize audio stuff
  window.audioout = new AudioOut(8192,function(e) {
    //creation callback
    window.synth = new Synth(e.sample_rate,64);
    window.player = new MIDIPlayer(e.sample_rate,receiveMIDI);
  },function (b1,b2) {
    //audio process callback
    for(var i=0;i<b1.length;i++) {
      synth.play();
      b1[i] = synth.outL;
      b2[i] = synth.outR;
      player.advance();
    }
  })

  //midi panic!!
  function panic() {     
    synth.kill_all_voices();
    synth.reset_all_channels();
  }

  //unsupported WebAudio API
  if (!window.audioout.enabled) {
    document.getElementById("content").style.cssText = "display: none";
    document.getElementById("unsupported").style.cssText = "display: block";
  }

  // Check for the various File API support.
if (window.File && window.FileReader && window.FileList && window.Blob) {

  //Setup the dnd listeners.
  window.dropZone = document.getElementById('drop_zone');

  function handleFileSelect(file) {
    dropZone.innerHTML = escape(file.name);
    var slice = file.slice(0,4);
    var reader = new FileReader();
    reader.readAsText(slice);
    reader.onload = function(e) {
      var buffer = reader.result;
//      var view = new DataView(buffer);
      if (buffer != "MThd") {
        alert ("This is not a MIDI File!");
        dropZone.innerHTML = "Drop file here";
      }
      else {
        window.localFile = file;
      }

    }

  }

  function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
  }

  function handleDragFileSelect(evt) {
    evt.preventDefault();
    var file = evt.dataTransfer.files[0];
    handleFileSelect(file);
  }

  function handleDialogFileSelect(evt) {
    evt.preventDefault();
    var file = evt.target.files[0];
    handleFileSelect(file);
  }

  function playRemoteFile() {
    panic();
    player.loadRemoteMIDIFile(document.getElementById('remoteuri').value);
  }

  function playLocalFile() {
    if (!window.localFile) {
      alert("Valid file not selected!");
    }
    else {
      panic();
      player.loadMIDIFile(window.localFile);      
    }
  }

  function setRemoteURIFromLink(evt) {
    evt.preventDefault();
    document.getElementById('remoteuri').value = evt.target.getAttribute("data-link");
  }

  dropZone.addEventListener('dragover', handleDragOver, false);
  dropZone.addEventListener('drop', handleDragFileSelect, false);

  document.getElementById('files').addEventListener('change', handleDialogFileSelect, false);

  document.getElementById('remoteuriload').addEventListener('mouseup',playRemoteFile, false);
  document.getElementById('localload').addEventListener('mouseup',playLocalFile, false);
  remoteselectors = document.getElementsByClassName('remoteselector')
  for(var i = 0; i < remoteselectors.length; i++)
  {
     remoteselectors.item(i).addEventListener('click',setRemoteURIFromLink, false);
  }

} else {
  alert('The File APIs are not fully supported in this browser.');
}


})();