(function(){

window.AudioOut = function (bufsize,creation,processor) {
  var me = this;
 this.shouldStop = true;
 this.bufsize = bufsize;
 this.enabled = false;
 this.context = null;
 this.processor = processor;

  if(!window.webkitAudioContext) {
    return;
  }
  if(window.webkitAudioContext && this.shouldStop == false) return;
    this.shouldStop = false;
  if(window.webkitAudioContext) {

    if(!this.context) this.context = new webkitAudioContext();
    this.node = this.context.createJavaScriptNode(bufsize, 0, 2);
    creation({sample_rate: this.context.sampleRate});
    this.node.onaudioprocess = function (e) {
      var buf1 = e.outputBuffer.getChannelData(0);
      var buf2 = e.outputBuffer.getChannelData(1);
      me.processor(buf1,buf2);      
      if(me.shouldStop) { me.node.disconnect(); }
    }
    this.node.connect(this.context.destination);
    this.enabled = true;
  }


};

})();
