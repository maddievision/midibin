(function(){

  window.MIDIPlayer = function(sample_rate,processor) {
    this.sample_rate = sample_rate;
    this.processor = processor;
  
    this.reset = function() {
      this.trk = [];
      this.tempo = 500000;
      this.ppqn = 96;
      this.evpos = 0;
      this.tpos = 0;
      this.playing = false;
      this.nextev = null;
      this.wpos = 1;
    };

    this.reset();

    this.advance = function() {
        if (this.playing) {
          this.wpos--;
          while (this.wpos <= 0) {
            this.playNextEvent();
          }
        }
    };

  this.loadMIDIFileFromBuffer = function ( buffer) {
      var ofs = 0;
      var r = struct.unpack("4sIHHH",buffer,ofs);
      ofs += 4 + 4 + 2 + 2 + 2;
      fmt = r[2];
      trackcount = r[3];
      ppqn = r[4];
      var bailout = false;
      var tracks = [];
      for (var i = 0 ; i < trackcount; i ++) {
        var rstat = 0;
        var events = [];
        var ts = 0;
        var ordid = 0;
        var r = struct.unpack("4sI",buffer,ofs);
        ofs += 4 + 4;
        var eot = false;
        while (!eot) {
          var skipevent = false;
          r = struct.unpack("B",buffer,ofs++);
          var vlq = r[0] & 0x7F;
          while (r[0] & 0x80) {
            r = struct.unpack("B",buffer,ofs++);
            vlq = (vlq << 7) + (r[0] & 0x7F);
          }
          ts += vlq;
          var mevent = [];
          r = struct.unpack("B",buffer,ofs++);
          s = r[0];
          if (s >= 0x80 && s <= 0xEF) {
            rstat = s;
            r = struct.unpack("B",buffer,ofs++);
            s = r[0]
          }
          else if (s == 0xFF) {
            mevent.push(s);
            r = struct.unpack("BB",buffer,ofs);
            ofs += 2;
            mt = r[0];
            mevent.push(mt);
            l = r[1];
            mevent.push(l);
            for (var j = 0; j < l; j++) {
              r = struct.unpack("B",buffer,ofs++);
              mevent.push(r[0]);
            }
            if (mt == 0x2F) {
              eot = true;
            }
          }
          else if (s == 0xF0 || s == 0xF7) {
//            alert("SYSEX unsupported at this time");
            skipevent = true;
            sn = -1;
            while (sn != 0xF7) {
              r = struct.unpack("B",buffer,ofs++);
              sn = r[0];
            }
          }
          else if ((s >= 0xF1 && s <= 0xF6) || (s >= 0xF8 && s <= 0xFE)) {
            alert('INVALID EVENT !' + s);
            bailout = true;
            eot = true;
          }

          if (s < 0x80) {
            mevent.push(rstat);
            mevent.push(s);
            if (rstat < 0xC0 || rstat > 0xDF) {
              r = struct.unpack("B",buffer,ofs++);
              mevent.push(r[0]);
            }
          }
          if (!skipevent) {
                      events.push([ts,ordid++,mevent]);
          }

        }
        if (bailout) return;

        tracks.push(events);
      }
      this.playMIDI(tracks,ppqn);
  }
  this.loadRemoteMIDIFile = function (uri) {
    var req = new XMLHttpRequest();
    req.open("GET", uri, true);
    req.responseType = "arraybuffer";
    var me = this;
    req.onload = function (oEvent) {
      var buffer = req.response;
      if (buffer) {
        me.loadMIDIFileFromBuffer(buffer);
      }
    };

    req.send(null);
  }
  this.loadMIDIFile = function( file ) {
    var reader = new FileReader();
    reader.readAsArrayBuffer(file);
    var me = this;
    reader.onload = function(e) {
      var buffer = reader.result;
      me.loadMIDIFileFromBuffer(buffer);
    }
  };
  this.playMIDI = function(tracks, ppqn) {
    this.reset();
    this.ppqn = ppqn;
    for (var i = 0; i < tracks.length; i++) {
      this.trk = this.trk.concat(tracks[i]);
    }
    this.trk.sort(function (a,b) {
      var na = (a[0] << 8) + a[1];
      var nb = (b[0] << 8) + b[1];
      if (na > nb) return 1;
      if (na < nb) return -1;
      return 0;
    });
//   window.trk = tracks[0];
    this.playing = true;
  };
  this.playNextEvent = function() {
 //   alert("pos "+evpos+" tracks size "+trk.length);
    if (this.nextev != null) {
      this.processMIDI(this.nextev);
      this.nextev = null;
    }

    if (this.evpos >= this.trk.length) {
      this.playing = false;
      return;
    }
    var ev = this.trk[this.evpos];
      this.evpos++;
    if (ev[0] == this.tpos) {
//        alert("proc 0");
//      alert("Got status " + ev[2]);

      this.processMIDI(ev[2]);
      this.playNextEvent();
    }
    else if (ev[0] > this.tpos) {
//        alert("proc 1");
 //     alert("Got status x " + ev[1]);
      var diff = ev[0] - this.tpos;
      this.tpos = ev[0];
      this.nextev = ev[2];
      this.wpos += (diff / this.ppqn) * this.tempo / 1000000 * this.sample_rate;
    }
  };

  this.processMIDI = function( ev ) {
    var status = ev[0];
    if (status == 0xFF) {
      if (ev[1] == 0x51) {
        this.tempo = (ev[3] << 16) + (ev[4] << 8) + ev[5];
      }
    }
    else {
      this.processor({data: ev});
    }
  }



  }

})();