(function(){

window.MIDIInput = function (processor) {

  this.midi = null
  this.midiInput = null;

  var me = this;
  if (navigator.requestMIDIAccess) {
    this.prm = navigator.requestMIDIAccess();
    if (this.prm) {
      this.prm.then( function(midiAccess) { 
        me.midi = midiAccess;
        var midiInputs = me.midi.inputs();
        var i = 0;
        for (i = 0; i < midiInputs.length; i++) {
          me.midiInput = me.midi.inputs()[i];
          me.midiInput.onmidimessage = me.processor;      
        }
      }, function(midiAccess) { });
    }
  }

};

})();
